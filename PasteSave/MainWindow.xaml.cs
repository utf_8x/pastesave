﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace PasteSave
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            savedLabel.Content = "";

            RoutedCommand paste = new RoutedCommand();
            paste.InputGestures.Add(new KeyGesture(Key.V, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(paste, OnPaste));
            
            RoutedCommand save = new RoutedCommand();
            save.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(save, OnSave));
        }

        private void OnPaste(object sender, ExecutedRoutedEventArgs e)
        {
            if (Clipboard.ContainsImage())
            {
                savedLabel.Content = "";
                labelPaste.Content = "";
                info.Content = "";
                BitmapSource bmp = Clipboard.GetImage();
                img.Source = bmp;
            }
        }

        private void OnSave(object sender, ExecutedRoutedEventArgs e)
        {
            if (img.Source == null) return;

            String date = DateTime.Now.ToString("mm-hh-ss_dd-MM-yyyy");
            String home = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            String saveFolder = $"{home}\\Pictures\\PasteSave";

            System.IO.Directory.CreateDirectory(saveFolder);

            using(var fileStream = new FileStream($"{saveFolder}\\screenshot_{date}.png", FileMode.Create))
            {
                BitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create((BitmapSource) img.Source));
                encoder.Save(fileStream);
            }

            img.Source = null;
            savedLabel.Content = "Saved!";
            labelPaste.Content = "Ready to paste a new image";
            info.Content = "CTRL+V To paste. CTRL+S To save. CTRL+P For settings.";
        }
    }
}
